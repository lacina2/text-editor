//---------------------------------------------------------------------------

#pragma hdrstop

#include "Dokument.h"
//---------------------------------------------------------------------------
TDocument::TDocument(TPageControl *APageControl): TTabSheet(APageControl)
{
	PageControl = APageControl;
	Caption = "Nowy dokument";
	RichEdit = new TRichEdit(this);
	RichEdit->Parent = this;
	RichEdit->Align = alClient;
	RichEdit->WordWrap = false;
	RichEdit->ScrollBars = ssBoth;
    RichEdit->OnChange = APageControl->OnChange;
}

void TDocument::LoadFromFile(String AFileName)
{
	FileName = AFileName;
	RichEdit->Lines->LoadFromFile(FileName);
	Caption = ExtractFileName(FileName);
}

__fastcall TDocument::~TDocument(){
	delete RichEdit;
}
#pragma package(smart_init)
