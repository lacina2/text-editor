//---------------------------------------------------------------------------

#ifndef DokumentH
#define DokumentH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.StdActns.hpp>
#include <Vcl.Dialogs.hpp>


class TDocument : public TTabSheet
{
 public:
	TDocument(TPageControl *APageControl);
	__fastcall ~TDocument();
	void LoadFromFile(String AFileName);
	TRichEdit *RichEdit;
	String FileName;
} ;


#endif
