//---------------------------------------------------------------------------

#pragma hdrstop

#include "TStringList.h"
//---------------------------------------------------------------------------

TStringList *Tokenize (String s,String delimiter)
{
	TStringList *StringList;
	int l; //length of s
	int start,delimp;
	String sub;
	StringList = new TStringList();
	start=0;
	if (s.Pos(delimiter)>0)
	{
	delimp = s.Pos(delimiter);
	do
	{
		l=s.Length();
		sub=s.SubString(start,delimp-1);
		if (sub != "") StringList->Add(sub);
		s=s.SubString(delimp+1,l-delimp);
		delimp = s.Pos(delimiter);
		} while (delimp != 0);
		StringList->Add(s); // dodanie tego co zosta�o na ko�cu
	}
	else StringList->Add(s);
#pragma package(smart_init)
